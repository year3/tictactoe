package com.ntc.lab01;

import java.util.Scanner;

/**
 *
 * @author L4ZY
 */
public class Board {

    char[][] board = new char[3][3];
    //create 3x3 board
    char winner = ' ';

    Scanner kb = new Scanner(System.in);
    
    int count = 1;//count round 
    
    public void Play() {//create play method

        while (count < 10) {

            if (count % 2 == 1) {
                System.out.println("Turn O");
                System.out.println("Please input row, col:");
                inputO(kb.nextInt(), kb.nextInt());
                print();
                checkwin();

            } else {
                System.out.println("Turn X");
                System.out.println("Please input row, col:");
                inputX(kb.nextInt(), kb.nextInt());
                print();
                checkwin();
            }
            if (check == false && count == 9) {
                System.out.println(">>>Draw<<<");

            } else if (check == true) {
                System.out.println(">>> " + Character.toUpperCase(winner) + " WIN <<<");
                break;
            }
            count++;
        }
    }

    public void fill() {// fill method for first play
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }

    public void print() {// print method 
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");

            }
            System.out.println("");
        }
    }

    public void inputO(int row, int col) { //inputO method
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[row - 1][col - 1] = 'o';
            }
        }
    }

    public void inputX(int row, int col) {  //inputX method
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[row - 1][col - 1] = 'x';
            }
        }
    }

    boolean check = false;
    public void checkwin() {//check winner
        //check row
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != '-') {
                check = true;
                winner = board[i][0];
            }
        }
        //check column
        for (int j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != '-') {
                check = true;
                winner = board[0][j];

            }
        }
        //check right diagonst diagonal
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-') {
            check = true;
            winner = board[0][0];

        }
        //check left diagonst diagonal
        if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != '-') {
            check = true;
            winner = board[0][2];

        }

    }
}
